import Link from 'next/link'


const Navbar = () => (
    <nav className='navbar'>
        <Link href='/'>
            <span className='navbar-brand'>Note App</span>
        </Link>
        <Link href='/new'>
            <span className='create'>Add Note</span>
        </Link>
    </nav>
)

export default Navbar